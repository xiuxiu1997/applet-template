# 小程序模板

#### 介绍
提供小程开发中常用的一些结构

#### 软件架构
..


#### 安装教程

1.  git clone 即可将项目拉至本地

#### 使用说明

1.  apis  -请求
    ```
    module -请求模块
    util -请求封装的工具
        urlConfig -请求地址
        request -请求封装
        util -请求工具类
    index -暴露接口
    ```
2.  components -组件
    ```
    common -自用组件库
    ```
3.  static -静态文件
    ```
    css -公共样式
    icon -图标
    image -图片
    ```
4.  subpages -分包
4.  utils -工具
    ```
    page -模板页面
    utils -工具类
    ```



