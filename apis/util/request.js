import {
  getToken,
  saveToken
} from './util'

const SUCCESS_CODE = 200;
const SFAIL_CODE = 400
const POWER_CODE = 403
const ERROR_CODE = 500

function request(BASE_URL, config) {
  return new Promise((resolve, reject) => {
    // 转大写
    config.method = config.method.toUpperCase()
    // 请求加载
    // wx.showLoading({
    //   title: '加载中...',
    // })
    const str = BASE_URL + config.url
    // 获取token
    var myData = getToken(str)
    wx.request({
      url: BASE_URL + config.url,
      data: config.data,
      method: config.method,
      header: {
        ...myData,
        ...config.header
      },
      success: async function (res) {
        saveToken(res.header)
        if (res.statusCode == SUCCESS_CODE) {
          // 成功-请求成功发起的回调
          resolve(res.data)
        } else if (res.statusCode == POWER_CODE) {
          // 失败-权限问题
          reject(res);

        } else if (res.statusCode == SFAIL_CODE) {
          // 失败-失败请求-请求中参数等，错误后端自定义错误
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
          reject(res);
        } else if (res.statusCode == ERROR_CODE) {
          // 失败-失败请求-请求异常
          reject(res);
        } else {
          // 失败-其他错误
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
          reject(res);
        }
      },
      error: function (e) {
        // 请求失败发起的回调
        console.log(e);
        reject("哎啊,请求失败了");
      },
      complete: () => {
        // wx.hideLoading()
      }
    });

  });
}
module.exports = request;