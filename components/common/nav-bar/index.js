// pages/navBar/navBar.js
import {
    list
} from './utils/nav'
console.log(list[0].pagePath)
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        bgc: {
            type: String,
            value: ''
        },
        color: {
            type: String,
            value: 'rgba(0, 0, 0, 1)'
        },
        titleText: {
            type: String,
            value: ''
        },
        titleImg: {
            type: String,
            value: ''
        },
        backIcon: {
            type: String,
            value: './icon/back-icon.png '
        },
        homeIcon: {
            type: String,
            value: './icon/home-icon.png'
        },
        fontSize: {
            type: Number,
            value: 16
        },
        iconHeight: {
            type: Number,
            value: 19
        },
        iconWidth: {
            type: Number,
            value: 58
        },
        shadowFlag: {
            type: Boolean,
            value: false
        },
        showIconFlag: {
            type: Boolean,
            value: true
        },
        userBack: {
            type: Boolean,
            value: true
        },
        isFixed: {
            type: Boolean,
            value: true
        },
        isFill: {
            type: Boolean,
            value: true
        }
    },
    attached: function () {
        var that = this
        that.setNavSize()
        that.setStyle()
        that.listFormatter()
    },
    data: {
        homeUrl: '/' + list[0].pagePath,
        navs: []
    },
    methods: {
        // navList数据格式化
        listFormatter: function () {
            this.data.navs = list.map(item => '/' + item.pagePath)
        },
        // 通过获取系统信息计算导航栏高度
        setNavSize: function () {
            var that = this,
                sysinfo = wx.getSystemInfoSync(),
                statusHeight = sysinfo.statusBarHeight,
                isiOS = sysinfo.system.indexOf('iOS') > -1,
                navHeight
            if (!isiOS) {
                navHeight = 48
            } else {
                navHeight = 44
            }
            that.setData({
                status: statusHeight,
                navHeight: navHeight
            })
        },
        setStyle: function () {
            var that = this,
                containerStyle,
                textStyle,
                iconStyle
            containerStyle = ['background-color:' + that.data.background].join(';')
            textStyle = [
                'color:' + that.data.color,
                'font-size:' + that.data.fontSize + 'px'
            ].join(';')
            iconStyle = [
                'width: ' + that.data.iconWidth + 'px',
                'height: ' + that.data.iconHeight + 'px'
            ].join(';')
            if (that.data.shadowFlag) {
                that.setData({
                    shadowStyle: 'box-shadow:0px 12px 8px -10px #f2f2f2;'
                })
            }
            that.setData({
                containerStyle: containerStyle,
                textStyle: textStyle,
                iconStyle: iconStyle
            })
        },
        // 返回事件
        back: function () {
            if (this.data.userBack) {
                const url = wx.getStorageSync('backUrl')
                let pages = getCurrentPages()
                if (pages.length == 1) {
                    if (pages[0].route === url) {
                        wx.switchTab({
                            url: this.data.homeUrl,
                        })
                    } else {
                        if (this.data.navs.findIndex(url) !== -1) {
                            wx.switchTab({
                                url: '/' + url,
                            })
                        } else if (!url) {
                            wx.switchTab({
                                url: this.data.homeUrl,
                            })
                        } else {
                            wx.redirectTo({
                                url: '/' + url,
                            })
                        }
                    }
                } else {
                    if (url) {
                        if (url == 'pages/mine/index' || url == 'pages/index/index' || url == 'pages/news/index') {
                            wx.switchTab({
                                url: '/' + url,
                            })
                        } else {
                            wx.redirectTo({
                                url: '/' + url
                            })
                        }
                    } else {
                        wx.navigateBack({
                            delta: 1
                        })
                    }

                }

            } else {
                this.triggerEvent('back', {})
            }



        },
        home: function () {
            console.log(this.data.homeUrl)
            wx.switchTab({
                url: this.data.homeUrl,
            })
            // this.triggerEvent('home', {})
        }
    }
})