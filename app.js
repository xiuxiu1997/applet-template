// app.js
import request from '/apis/index.js'
App({
  onLaunch() {

  },
  // 获取设备信息
  getPhoneSys() {
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.globalData.windowHeight = res.windowHeight
        that.globalData.windowWidth = res.windowWidth
        let isiOS = res.system.indexOf('iOS') > -1
        let statusHeight = res.statusBarHeight
        let navHeight = 0
        if (!isiOS) {
          navHeight = 48
        } else {
          navHeight = 44
        }
        that.globalData.navHeight = navHeight + statusHeight

        if (res.model.search('iPhone X') != -1) {
          that.globalData.isIphoneX = true
        }
      }
    })
  },
  // 小程序自动登录
  autoLogin() {
    wx.login({
      complete: res => {
        if (res.errMsg === 'login:ok') {
          //  获取code res.code
        }
      }
    })
  },
  // 请求
  $api: request,
  globalData: {
    userInfo: null,
    navHeight: 0, //  状态栏高度
  }
})