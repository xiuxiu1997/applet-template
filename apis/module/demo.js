import request from '../util/request.js'
import {
  mainUrl,
  APIURL
} from '../util/urlConfig'
let url = mainUrl + APIURL

module.exports = {
// 小程序自动登录
  appLoginAuto(data) {
    return request(url, {
      url: `/login`,
      method: "post",
      data: data,
    })
  },
}