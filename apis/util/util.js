// 接口中的一些方法

let headerTokenStr = 'access-token' // 服务器端给的token的标识
let saveTokenStr = 'token' // token存储的标识

// 获取token
function getToken() {
  let myData = {}
  let token = wx.getStorageSync(saveTokenStr)
  if (token) {
    myData[headerTokenStr] = token
  }
  return myData
}

// 保存token
function saveToken(res) {
  if (res[headerTokenStr]) {
    wx.setStorageSync(saveTokenStr, res[headerTokenStr])
  }
}
module.exports = {
  headerTokenStr,
  saveTokenStr,
  getToken,
  saveToken
}